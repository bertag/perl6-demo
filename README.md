# Introduction

Perl was originally developed by Larry Wall in 1987 as a general-purpose Unix scripting language to make report processing easier.  It should be recognized that Larry Wall was twice the world champion of the International Obfuscated C Code Contest, and I will leave it up to you to decide whether this distinction has influenced the Perl language at all!  ;)

Like many languages, Perl went through a period of rapid development in its early days.  Perl 2 and Perl 3, released in 1988 and 1989 respectively, added a better regular expression engine and support for binary data streams.  Until this point, the only documentation for Perl was a single (increasingly lengthy) man page. In 1991, Programming Perl, known to many Perl programmers as the "Camel Book" because of its cover, was published and became the *de facto* reference for the language. At the same time, the Perl version number was bumped to 4, not to mark a major change in the language but to identify the version that was well documented by the book.

Perl 5, released in 1994, marked a major transition point for the language, adding a host of new features including objects, references, and modules.  For the first time, the language could be extended (through modules) without modifying the interpreter itself.  This allowed the core interpreter to finally stabilize, and led to the "golden age" of Perl and the rise of the CPAN package repository.

No written specification or standard for the Perl language existed for Perl versions through Perl 5, and there are no plans to create one.  There has been only one implementation of the interpreter, and the language has evolved along with with it.  That interpreter, along with its functional tests, stands as a *de facto* specification fof the language.

In 2000, following the release of Perl 5.6 (which added 64-bit and unicode support among other things), Larry Wall put forth a call for suggestions for a new version of Perl from the community.  This began a 15 year long effort to develop a new modern version of Perl, which was finally released in 2015 as Perl 6.

Perl 6 is considered to be a separate language from its predecessors, and backwards compatability is not supported.  However, clear attempts have been made to "keep Perl 6 Perl" and the language will look distinctly familiar to those already comfortable with Perl 5.  It is also worth noting that the separate development teams backing Perl 6 and Perl 5 openly and liberally copy ideas and libraries from each other, and the long development of Perl 6 was marked with some major advances in its older sibling.

Unlike its predecessors, Perl 6 began as and is a formal specification.  Throughout the 15 year development process, there were multiple implementations that were developed.  However, there is now only one major implementation being actively developed, Rakudo Perl 6.  This implementation compiles to both the MoarVM and the JVM.  Rakudo on MoarVM is considered better for short running processes, offering the best startup time and lowest memory usage of any Perl 6 implementation.  It is also the most complete implementation of the Perl 6 specification (passing 3 more tests than Rakudo on the JVM).  However, according to the MoarVM website itself, "for long-running workloads where startup time doesn't matter and you need tried-and-tested threading support, Rakudo on the JVM may be a better bet.  It may also - once the slow startup time is overcome - win on performance for some workloads, once its sophisticated JIT [just in time] compiler gets to work."

# Differences from "old-school" Perl

## Static types
Perl 6 adds support for static types. However, the dynamic typing of earlier Perl versions is still supported, and Perl 6 is thus strictly defined as a "gradually typed" language.

```
#!perl6
my $a = 0;  # Dynamic typing...
my $b = "hello";  # I pity the fool.

my Int $c = 1;  # New static typing!
my Str $d = "world";  # A spectacle of graphics and sound (and sanity).
```

## Formal subroutine parameter lists
Earlier versions of Perl had no formal subroutine definitions, and arguments were simply passed in as a mutable array.  Perl 6 finally adds support for true subrouting parameters, which are now constant by default (though they can be explicitly made mutable still).

```
#!perl6
sub do_something(Str $a, Int $b) {
    # ...
}
```

Perl 6 also provides very flexible mechanisms for passing arguments into subroutines.  Positional, named, and variadic ("slurpy") parameters are all supported.

```
#!perl6
sub somefunction($a, $b, :$c, :$d, *@e) {
    # ...
}

somefunction(1, 2, :d(3), 4, 5, 6); # $a=1, $b=2, $d=3, @e=(4,5,6)
somefunction(:b<2>, :a<1>);         # $a="1", $b="2"
```

## Closures/Lambdas

Perl 6 adds support for closures, and in fact this syntax is how `for` and `while` loops are named.

```
#!perl6
my @times = ();
for 1..10 {
    my $t = $_;
    # Each element of the array is populated with a closure/lambda expression that multiplies it by an argument
    # which will be supplied later...
    @times[$_] = sub ($a) {
        return $a * $t;
    };
}

say @times[3](4);
say @times[5](20);
say @times[7](3);
```

## Sigil Invariance

Perl is notorious for its "sigils", the punctuation characters which precede every variable reference.

| Sigil | Meaning |
| ----- | ------- |
| `$` | Scalar (single-value) variable |
| `@` | List/array variable |
| `%` | Dictionary/map variable |
| `&` | Callable variable (function or method) |

In earlier versions of Perl, the sigil required to dereference a variable changed depending on how the variable was used, but in Perl 6 sigils are now invariant.  Once a variable is declared, the corresponding sigil is consistent for all future references to that variable.

```
#!perl
# Sigil usage in Perl 5 and earlier...
my @array = ('a', 'b', 'c');
my $element = $array[1];
my @partial_array = @array[1, 2];
```

```
#!perl6
# Sigil usage in Perl 6.
my @array = ('a', 'b', 'c');
my $element = @array[1];
my @partial_array = @array[1, 2];
```

## Better Objects

Perl 5 added support for object-oriented programming via a mechanism known as "blessing".  While extremely powerful and flexible, this unique mechanism often made the most common object operations unnecessarily difficult and unoptimizable.

Perl 6 retains the old "blessing" model while also adding a more robust and concise object model for common use case:

```
#!perl6

class Point is rw {
    has $.x;
    has $.y;
}
```

my $point = Point.new( x => 1.2, y => -3.7 );
$point.x = 2  # Setter
say "Point has X coordinate: ", $point.x;

Accessor methods are created automatically, but can be replaced by user-defined methods if a more precise interface is needed.  All class members/attributes are private...always.

## Roles (Interfaces)

Perl 6 adds support "roles", which serve a similar function as Java interfaces.

## String Parsing and Processing

Perl's regular expression and string-processing support has always been one of its defining features.  For a number of years, the "regular expression" support in Perl has actually exceeded the capabilities of other regular language expressions.  Perl 6 formalizes this unique capability by referring to this extended functionality as "regexen" to distinguish it from weaker, but more universal "regex".  There is also better support for non-regular grammars.

## Syntactic Simplifaction

Certain constructs have been simplified when the meaning is inherantly unambiguous:

```
#!perl6
my @array = (1, 2, 3, 4, 5);
if (is_true()) {
    for (@array) {
        # ...
    }
}

# Can now also be written as:
my @array = 1, 2, 3, 4, 5;
if is_true() {
    for @array {
        # ...
    }
}
```

## Chained Comparisons

Perl 6 allows comparisons to "chain".

```
#!perl6
if 20 <= $temp <= 25 {
    say "Temperature is between 20 and 25!";
}
```

## Lazy Evaluation
Perl expressions are now lazily evaluated.  Among other benefits, this means that the following declaration will not crash your computer:

```
#!perl6
@integers = 0..Inf;  # integers from 0 to infinity

# "gather" and "take", similar to generators in Python, allow for lazy transformations too.
my $squares = gather for @integers {
    take $_ * $_;  # the result will be an infinite list of square numbers, but computed only when they are accessed.
};
```

## Junctions

Perl 6 allows for values which are composites of other values, as demonstrated below:

```
#!perl6
my $color = 'white';
unless $color eq 'white' | 'black' | 'gray' | 'grey' {
    die "Color is not grayscale\n";
}

# Is equivalent to:
unless $color eq 'white' || $color eq 'black' || $color eq 'gray' || color eq 'grey' {
    die "Color is not grayscale\n";
}
```

# Demo Time
Start a docker container:

```
#!bash
docker run -it --entryPoint=/bin/bash rakudo-star
```

Some initial prep **inside** the docker container:

```
#!bash
apt-get update
apt-get -y install vim
mkdir -p /home/perl6
cd /home/perl6
```

# Resources
- https://learnxinyminutes.com/docs/perl6/
- https://en.wikipedia.org/wiki/Perl_6
- https://en.wikibooks.org/wiki/Perl_6_Programming